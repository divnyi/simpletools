# SimpleTools

[![Version](https://img.shields.io/cocoapods/v/SimpleTools.svg?style=flat)](https://cocoapods.org/pods/SimpleTools)
[![License](https://img.shields.io/cocoapods/l/SimpleTools.svg?style=flat)](https://cocoapods.org/pods/SimpleTools)
[![Platform](https://img.shields.io/cocoapods/p/SimpleTools.svg?style=flat)](https://cocoapods.org/pods/SimpleTools)

## Example

Good example of usage is https://gitlab.com/divnyi/architecturesample

## Installation

SimpleTools is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SimpleTools'
```
