import UIKit

/// Value that can be changed, and whose changes can be observed
public class Observable<T>: EventStream<T> {
    public var value: T {
        didSet {
            self.notifyObservers()
        }
    }
    
    public init(_ value: T) {
        self.value = value
    }
    
    public func notifyObservers() {
        self.send(obj: self.value)
    }
    
    override public func observe(_ observer: @escaping (T) -> Void) -> Disposal {
        observer(self.value)
        return super.observe(observer)
    }
}
