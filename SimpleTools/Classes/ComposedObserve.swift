import UIKit

// swiftlint:disable function_parameter_count
// swiftlint:disable line_length

/// Contains a bunch of methods to join two EventStream into single observe block.
/// Can have up to 5 parameters
public final class ComposedObserve {
    public static func observe<T1>(_ obj1: EventStream<T1>,
                                   observer: @escaping ((T1) -> Void)) -> Disposal {
        return obj1.observe(observer)
    }
    
    public static func observe<T1, T2>(optionalObserver: @escaping ((T1?, T2?) -> Void),
                                       _ obj1: EventStream<T1>,
                                       _ obj2: EventStream<T2>) -> Disposal {
        var storedObj1: T1? = (obj1 as? Observable<T1>)?.value
        var storedObj2: T2? = (obj2 as? Observable<T2>)?.value
        var disposal: Disposal = []
        func call() { optionalObserver(storedObj1, storedObj2); return }
        disposal += obj1.observe { (obj1) in
            storedObj1 = obj1
            call()
        }
        disposal += obj2.observe { (obj2) in
            storedObj2 = obj2
            call()
        }
        return disposal
    }
    public static func observe<T1, T2>(_ obj1: EventStream<T1>,
                                       _ obj2: EventStream<T2>,
                                       strongObserver: @escaping ((T1, T2) -> Void)) -> Disposal {
        return observe(optionalObserver: { (t1, t2) in
            if let t1 = t1, let t2 = t2 {
                strongObserver(t1, t2)
            }
        }, obj1, obj2)
    }
    
    public static func observe<T1, T2, T3>(optionalObserver: @escaping ((T1?, T2?, T3?) -> Void),
                                           _ obj1: EventStream<T1>,
                                           _ obj2: EventStream<T2>,
                                           _ obj3: EventStream<T3>) -> Disposal {
        var storedObj1: T1? = (obj1 as? Observable<T1>)?.value
        var storedObj2: T2? = (obj2 as? Observable<T2>)?.value
        var storedObj3: T3? = (obj3 as? Observable<T3>)?.value
        func call() { optionalObserver(storedObj1, storedObj2, storedObj3); return }
        var disposal: Disposal = []
        disposal += obj1.observe { (obj1) in
            storedObj1 = obj1
            call()
        }
        disposal += obj2.observe { (obj2) in
            storedObj2 = obj2
            call()
        }
        disposal += obj3.observe { (obj3) in
            storedObj3 = obj3
            call()
        }
        return disposal
    }
    public static func observe<T1, T2, T3>(_ obj1: EventStream<T1>,
                                           _ obj2: EventStream<T2>,
                                           _ obj3: EventStream<T3>,
                                           strongObserver: @escaping ((T1, T2, T3) -> Void)) -> Disposal {
        return observe(optionalObserver: { (t1, t2, t3) in
            if let t1 = t1, let t2 = t2, let t3 = t3 {
                strongObserver(t1, t2, t3)
            }
        }, obj1, obj2, obj3)
    }
    
    public static func observe<T1, T2, T3, T4>(optionalObserver: @escaping ((T1?, T2?, T3?, T4?) -> Void),
                                               _ obj1: EventStream<T1>,
                                               _ obj2: EventStream<T2>,
                                               _ obj3: EventStream<T3>,
                                               _ obj4: EventStream<T4>) -> Disposal {
        var storedObj1: T1? = (obj1 as? Observable<T1>)?.value
        var storedObj2: T2? = (obj2 as? Observable<T2>)?.value
        var storedObj3: T3? = (obj3 as? Observable<T3>)?.value
        var storedObj4: T4? = (obj4 as? Observable<T4>)?.value
        func call() { optionalObserver(storedObj1, storedObj2, storedObj3, storedObj4); return }
        var disposal: Disposal = []
        disposal += obj1.observe { (obj) in
            storedObj1 = obj
            call()
        }
        disposal += obj2.observe { (obj) in
            storedObj2 = obj
            call()
        }
        disposal += obj3.observe { (obj) in
            storedObj3 = obj
            call()
        }
        disposal += obj4.observe { (obj) in
            storedObj4 = obj
            call()
        }
        return disposal
    }
    public static func observe<T1, T2, T3, T4>(_ obj1: EventStream<T1>,
                                               _ obj2: EventStream<T2>,
                                               _ obj3: EventStream<T3>,
                                               _ obj4: EventStream<T4>,
                                               strongObserver: @escaping ((T1, T2, T3, T4) -> Void)) -> Disposal {
        return observe(optionalObserver: { (t1, t2, t3, t4) in
            if let t1 = t1, let t2 = t2, let t3 = t3, let t4 = t4 {
                strongObserver(t1, t2, t3, t4)
            }
        }, obj1, obj2, obj3, obj4)
    }
    
    public static func observe<T1, T2, T3, T4, T5>(optionalObserver: @escaping ((T1?, T2?, T3?, T4?, T5?) -> Void),
                                                   _ obj1: EventStream<T1>,
                                                   _ obj2: EventStream<T2>,
                                                   _ obj3: EventStream<T3>,
                                                   _ obj4: EventStream<T4>,
                                                   _ obj5: EventStream<T5>) -> Disposal {
        var storedObj1: T1? = (obj1 as? Observable<T1>)?.value
        var storedObj2: T2? = (obj2 as? Observable<T2>)?.value
        var storedObj3: T3? = (obj3 as? Observable<T3>)?.value
        var storedObj4: T4? = (obj4 as? Observable<T4>)?.value
        var storedObj5: T5? = (obj5 as? Observable<T5>)?.value
        func call() { optionalObserver(storedObj1, storedObj2, storedObj3, storedObj4, storedObj5); return }
        var disposal: Disposal = []
        disposal += obj1.observe { (obj) in
            storedObj1 = obj
            call()
        }
        disposal += obj2.observe { (obj) in
            storedObj2 = obj
            call()
        }
        disposal += obj3.observe { (obj) in
            storedObj3 = obj
            call()
        }
        disposal += obj4.observe { (obj) in
            storedObj4 = obj
            call()
        }
        disposal += obj5.observe { (obj) in
            storedObj5 = obj
            call()
        }
        return disposal
    }
    public static func observe<T1, T2, T3, T4, T5>(_ obj1: EventStream<T1>,
                                                   _ obj2: EventStream<T2>,
                                                   _ obj3: EventStream<T3>,
                                                   _ obj4: EventStream<T4>,
                                                   _ obj5: EventStream<T5>,
                                                   strongObserver: @escaping ((T1, T2, T3, T4, T5) -> Void)) -> Disposal {
        return observe(optionalObserver: { (t1, t2, t3, t4, t5) in
            if let t1 = t1, let t2 = t2, let t3 = t3, let t4 = t4, let t5 = t5 {
                strongObserver(t1, t2, t3, t4, t5)
            }
        }, obj1, obj2, obj3, obj4, obj5)
    }
}
