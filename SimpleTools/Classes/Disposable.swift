import Foundation

/// Should use this one with += operator,
/// and just clean when objects are no longer needed
public typealias Disposal = [Disposable]

public class Disposable {
    private let dispose: () -> Void
    
    init(_ dispose: @escaping () -> Void) {
        self.dispose = dispose
    }
    
    deinit {
        dispose()
    }
}
