import UIKit

public typealias ArrayUpdate = KeyedUpdate<Int>

public typealias LazyArray1d<T> = LazyArray<T, Int, () -> Int>

/// Structure that provides all the relevant information to maintain
/// and update UITableView structure without sections
public class ArrayProvider<T>: NSObject {
    public let arr: LazyArray1d<T>
    public let partialUpdate: EventStream<ArrayUpdate>
    public let fullUpdate: EventStream<Void>
    
    public init(arr: LazyArray1d<T>, partialUpdate: EventStream<ArrayUpdate>, fullUpdate: EventStream<Void>) {
        self.arr = arr
        self.partialUpdate = partialUpdate
        self.fullUpdate = fullUpdate
    }
    
    public func transform<NewType>(_ transform: @escaping (T) -> NewType) -> ArrayProvider<NewType> {
        return ArrayProvider<NewType>(arr: self.arr.transform(transform),
                                      partialUpdate: self.partialUpdate,
                                      fullUpdate: self.fullUpdate)
    }
}
