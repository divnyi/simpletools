//
//  EventStream.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/8/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

import Foundation

/// Stream of events that can be observed
public class EventStream<T> {
    public typealias Observer = (T) -> Void
    
    public var disposal: Disposal = [] // for the case where we create one from other...
    private var observers: [Int: Observer] = [:]
    private var uniqueID = (0...).makeIterator()
    
    public init () {
    }
    
    public func send(obj: T) {
        self.observers.values.forEach { $0(obj) }
    }
    
    public func observe(_ observer: @escaping Observer) -> Disposal {
        let id = uniqueID.next()!
        
        observers[id] = observer
        
        let disposable = Disposable { [weak self] in
            self?.observers[id] = nil
        }
        
        return [disposable]
    }
    
    public func removeAllObservers() {
        observers.removeAll()
    }
}
