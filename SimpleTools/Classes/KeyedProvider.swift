import UIKit
import SimpleTools

public class KeyedUpdate<Key> where Key: Hashable {
    public var insertions = Set<Key>()
    public var deletions = Set<Key>()
    public var updates = Set<Key>()
    public init() {}
    public init(insertions: Set<Key>, deletions: Set<Key>, updates: Set<Key>) {
        self.insertions = insertions
        self.deletions = deletions
        self.updates = updates
    }
}

public typealias KeyedLazyArray<Key, Value> = LazyArray<Value, Key, () -> Int>

/// Structure that provides and updates values by keys
///
/// Keys should be unique identifiers
///
/// KeyedProvider doesn't provide sequential access - you can't iterate from 0 to arr.count
///
/// Can produce key observables, because unique key guarantees key won't change by insertions or deletions
public class KeyedProvider<Key, Value>: NSObject where Key: Hashable {
    public let arr: KeyedLazyArray<Key, Value>
    public let partialUpdate: EventStream<KeyedUpdate<Key>>
    public let fullUpdate: EventStream<Void>
    
    public init(arr: KeyedLazyArray<Key, Value>,
                partialUpdate: EventStream<KeyedUpdate<Key>>,
                fullUpdate: EventStream<Void>) {
        self.arr = arr
        self.partialUpdate = partialUpdate
        self.fullUpdate = fullUpdate
    }
    
    public func transform<NewValue>(_ transform: @escaping (Value) -> NewValue) -> KeyedProvider<Key, NewValue> {
        return KeyedProvider<Key, NewValue>(arr: self.arr.transform(transform),
                                            partialUpdate: self.partialUpdate,
                                            fullUpdate: self.fullUpdate)
    }
    
    /// Observe certain key
    ///
    /// If object for key is not present (initially or deleted)
    /// value can be nil
    public func observe(key: Key) -> Observable<Value?> {
        // swiftlint:disable:next force_try
        let currentValue = try! self.arr.boundarySafeItem(idx: key)
        let observable = Observable(currentValue)
        
        observable.disposal += self.fullUpdate.observe { [weak observable, weak self] in
            // swiftlint:disable:next force_try
            observable?.value = try! self?.arr.boundarySafeItem(idx: key)
        }
        observable.disposal += self.partialUpdate.observe { [weak observable, weak self] (update) in
            if  update.updates.contains(key) ||
                update.insertions.contains(key) ||
                update.deletions.contains(key) {
                // swiftlint:disable:next force_try
                observable?.value = try! self?.arr.boundarySafeItem(idx: key)
            }
        }
        
        return observable
    }
}
