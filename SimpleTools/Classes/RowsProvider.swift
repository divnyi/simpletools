import UIKit

public typealias RowsUpdate = (sections: KeyedUpdate<Int>, rows: KeyedUpdate<IndexPath>)

public typealias LazyArray2d<T> = LazyArray<T, IndexPath, (sections: () -> Int, rows: (Int) -> Int )>

/// Structure that provides all the relevant information to maintain
/// and update UITableView structure with sections
public class RowsProvider<T>: NSObject {
    public let arr: LazyArray2d<T>
    public let partialUpdate: EventStream<RowsUpdate>
    public let fullUpdate: EventStream<Void>
    
    public init(arr: LazyArray2d<T>, partialUpdate: EventStream<RowsUpdate>, fullUpdate: EventStream<Void>) {
        self.arr = arr
        self.partialUpdate = partialUpdate
        self.fullUpdate = fullUpdate
    }
    
    public func transform<NewType>(_ transform: @escaping (T) -> NewType) -> RowsProvider<NewType> {
        return RowsProvider<NewType>(arr: self.arr.transform(transform),
                                     partialUpdate: self.partialUpdate,
                                     fullUpdate: self.fullUpdate)
    }
}
