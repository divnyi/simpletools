//
//  LazyArray.swift
//  ArchitectureSample
//
//  Created by Oleksii Horishnii on 1/4/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

public enum LazyArrayErrors: Error {
    case outOfBounds
}

public class LazyArray<Type, IndexType, CountFnType>: NSObject {
    public var count: CountFnType
    private var itemFn: ((IndexType) throws -> Type)
    
    public init(count: CountFnType,
                item: @escaping ((IndexType) throws -> Type)) {
        self.itemFn = item
        self.count = count
    }
    
    public func item(idx: IndexType) throws -> Type {
        return try self.itemFn(idx)
    }
    public func boundarySafeItem(idx: IndexType) throws -> Type? {
        do {
            return try self.itemFn(idx)
        } catch LazyArrayErrors.outOfBounds {
            return nil
        }
    }
    public subscript(idx: IndexType) -> Type {
        // swiftlint:disable:next force_try
        return try! self.item(idx: idx)
    }
    public subscript(safe idx: IndexType) -> Type? {
        return try! self.boundarySafeItem(idx: idx)
    }
    
    public func transform<NewType>(_ transform: @escaping (Type) -> NewType) ->
        LazyArray<NewType, IndexType, CountFnType> {
        let retval = LazyArray<NewType, IndexType, CountFnType>(count: self.count) { (idx) -> NewType in
            return transform(self[idx])
        }
        return retval
    }
}
